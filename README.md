# Mobiphone_Website


## Introduction
This repository is dedicated to the Mobiphone Website. For more information or the project description check the [wiki](https://gitlab.com/mobiphone_website/mobiphone_website/wikis/Project). 
If you have any questions contact your project manager or <development@mobiphone.nl>.
Most information can be found in the readme, the readme will be continuously updated. 
## Guidelines
The master branch contains the most recent production(ready) version. No one works directly in the master branch, create a new branch for each addition.
### Branches
Developers working on new parts/features/issues of the project will have to create a new branch. 
The developer can create a merge request after completion of the work. The work will be reviewed by our inhouse developers and feedback might be given. 

#### Upvoting System.

If there is no more feedback and all the work is done /requirements are met then the merge request will receive an update from our inhouse developers.<br />
<strong> The merge request can be executed after receiving at least two upvotes</strong>

## Definition of Done.
The following section contains some coding deadlines to make sure that the repository is as clean as possible, easy comprehensible and maintable.

*  Please validate your HTML with https://validator.w3.org/
*  Please validate your CSS with http://jigsaw.w3.org/css-validator/
*  Please spell check your variable names, class names etc.
*  Please do not use single char variable names, for example ``` int i = index ``` will not be accepted
*  Please make sure that all tags are closed
*  Please make sure that the doctype is declared
*  Please use comments when necessary to make sure the work is maintainable by other developers
*  Please check responsiveness,adaptiveness(if requested)

## Questions?
Any questions or feedback please let us know either through contacting your project supervisor or the developer <development@mobiphone.nl>